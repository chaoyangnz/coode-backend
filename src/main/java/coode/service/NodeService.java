package coode.service;

import coode.dto.NodeDetails;
import coode.entity.Tag;
import coode.entity.User;
import coode.repository.NodeRepository;
import coode.dto.NodeForm;
import coode.entity.Node;
import coode.repository.TagRepository;
import coode.repository.UserRepository;
import coode.support.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class NodeService {

    @Inject
    private NodeRepository nodeRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private TagRepository tagRepository;


    public NodeDetails queryNodeDetails(Long id) {
        Node node = nodeRepository.findOne(id);

        NodeDetails nodeDetails = Utils.map(node, NodeDetails.class);

        return nodeDetails;
    }

    public void createNode(NodeForm form) {
        Node node = Utils.map(form, Node.class);
        node.setCreatedAt(Utils.now());
        User creator = userRepository.findOne(1L);
        node.setCreator(creator);
        node.setCommentCount(0);

        String[] tagNames = form.getTagNames().split(",");
        List<Tag> tags = new ArrayList<>();
        for(String tagName : tagNames) {
            Tag tag = tagRepository.findFirstByName(tagName);
            if(tag == null) {
                tag = new Tag();
                tag.setName(tagName);
                tag.setCreatedBy(1L);
            }
            tags.add(tag);
        }
        node.setTags(tags);

        nodeRepository.save(node);
    }

    public List<NodeDetails> queryAll() {

        List<Node> nodes = nodeRepository.findAll();
        return Utils.mapList(nodes, NodeDetails.class);
    }
}
