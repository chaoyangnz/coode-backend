package coode.controller;

import coode.dto.NodeDetails;
import coode.service.NodeService;
import coode.dto.NodeForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.util.List;

@RestController
@RequestMapping("nodes")
public class NodeController {

    @Autowired
    private NodeService nodeService;

    @RequestMapping(method = GET)
    public List<NodeDetails> list() {
        return nodeService.queryAll();
    }

    @RequestMapping(method = POST)
    public void create(@RequestBody NodeForm node) {

        nodeService.createNode(node);
    }

    @RequestMapping(value = "/{id}", method = PUT)
    public void update(@PathVariable Long id, @RequestBody NodeForm node) {

    }

    @RequestMapping(value = "/{id}", method = GET)
    public NodeDetails query(@PathVariable Long id) {
        return nodeService.queryNodeDetails(id);
    }

    @ExceptionHandler(Exception.class)
    public void onError(Exception ex) {
        ex.printStackTrace();
    }
}
